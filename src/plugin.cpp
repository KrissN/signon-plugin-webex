/*
 * This file is part of signon WebEx plugin
 *
 * Copyright (C) 2016 Krzysztof Nowicki <krissn@op.pl>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "plugin.h"

#include <QtNetwork/QNetworkReply>
#include <QtXml/QDomDocument>
#include <QtNetwork/QNetworkAccessManager>
#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <SignOn/Error>

#include "plugindata.h"

using namespace SignOn;

namespace SignOnWebex {

SIGNON_DECL_AUTH_PLUGIN(Plugin)

class PluginPrivate : public QObject
{
    Q_OBJECT
public:
    PluginPrivate(Plugin *parent);
    virtual ~PluginPrivate();

    void retrieveJabberToken();
    void performWebexSSO();
public Q_SLOTS:
    void onSwapiLoginDone(QNetworkReply *reply);

public:
    QScopedPointer<QNetworkReply> mReply;
    PluginTokenData mTokenData;
    QString mUsername;
    QString mPassword;
    bool mTriedSSO;

    Plugin *q_ptr;
    Q_DECLARE_PUBLIC(Plugin);
};

PluginPrivate::PluginPrivate(Plugin *parent)
    : q_ptr(parent), mTriedSSO(false)
{
}

PluginPrivate::~PluginPrivate()
{
}

void PluginPrivate::retrieveJabberToken()
{
    QUrl url = QUrl(QStringLiteral("https://") + mTokenData.SWAPIServer() + QStringLiteral("/op.do"));
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("cmd"), QStringLiteral("login"));
    query.addQueryItem(QStringLiteral("autocommit"), QStringLiteral("true"));
    query.addQueryItem(QStringLiteral("username"), mUsername);
    query.addQueryItem(QStringLiteral("isp"), QStringLiteral("WBX"));
    query.addQueryItem(QStringLiteral("token"), QUrl::toPercentEncoding(mTokenData.Token()));
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished, this, &PluginPrivate::onSwapiLoginDone);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    manager->post(request, query.toString(QUrl::FullyEncoded).toLatin1());
}

void PluginPrivate::onSwapiLoginDone(QNetworkReply *reply)
{
    Q_Q(Plugin);

    if (reply->error() != QNetworkReply::NoError) {
        emit q->error(Error(Error::ServiceNotAvailable, QStringLiteral("SWAPI network error %1").arg(reply->error())));
        return;
    }

    QDomDocument doc;
    if (!doc.setContent(reply)) {
        emit q->error(Error(Error::MissingData, QStringLiteral("SWAPI reply parse")));
        return;
    }

    QDomNode root = doc.firstChildElement(QStringLiteral("wbxapi"));

    if (root.firstChildElement(QStringLiteral("response")).firstChildElement(QStringLiteral("result")).text().trimmed()
            != QStringLiteral("SUCCESS")) {
        QDomElement msgElm = root.firstChildElement(QStringLiteral("messageStrings")).firstChildElement(QStringLiteral("message"));
        while (!msgElm.isNull()) {
            qDebug() << "WBXAPI Error:" << msgElm.text().trimmed();
            msgElm = msgElm.nextSiblingElement(QStringLiteral("message"));
        }

        if (!mTriedSSO) {
            /* Reusing an existing token failed. Fall back to full SSO to retrieve a new one. */
            mTriedSSO = true;
            performWebexSSO();
        } else {
            QString reason = root.firstChildElement(QStringLiteral("response")).firstChildElement(QStringLiteral("reason")).text().trimmed();
            emit q->error(Error(Error::NotAuthorized, QStringLiteral("SWAPI authentication failed: ") + reason));
        }
        return;
    }

    QString token = root.firstChildElement(QStringLiteral("securityContext"))
        .firstChildElement(QStringLiteral("additionalInfo"))
            .firstChildElement(QStringLiteral("jabberToken")).text().trimmed();

    mTokenData.setJabberToken(token);

    Q_EMIT q->store(mTokenData);

    Q_EMIT q->result(mTokenData);

    reply->deleteLater();
}


void PluginPrivate::performWebexSSO()
{
    Q_Q(Plugin);

    UiSessionData uiData;
    int pos = mUsername.indexOf('@');
    uiData.setOpenUrl(QStringLiteral("https://loginp.webexconnect.com/cas/FederatedSSO?org=%2&type=connect2").arg(mUsername.mid(pos+1)));
    uiData.setFinalUrl(QStringLiteral("https://loginp.webexconnect.com/cas/SAML2AuthService"));
    uiData.setUserName(mUsername);
    uiData.setSecret(mPassword);
    uiData.setRequestWebContent(true);
    emit q->userActionRequired(uiData);
}

Plugin::Plugin(QObject *parent)
    : AuthPluginInterface(parent), d_ptr(new PluginPrivate(this))
{
}
    
Plugin::~Plugin()
{
}

QString Plugin::type() const
{
    return QStringLiteral("webex");
}

QStringList Plugin::mechanisms() const
{
    return QStringList() << QStringLiteral("token");
}

void Plugin::cancel()
{
    Q_D(Plugin);

    emit error(Error(Error::SessionCanceled));
    if (d->mReply) {
        d->mReply->abort();
    }
}

void Plugin::process(const SignOn::SessionData &inData, const QString &mechanism)
{
    Q_D(Plugin);

    if ((!mechanism.isEmpty()) && (!mechanisms().contains(mechanism))) {
        emit error(Error(Error::MechanismNotAvailable));
        return;
    }

    d->mTokenData = inData.data<PluginTokenData>();

    if (inData.UiPolicy() == RequestPasswordPolicy) {
        d->mTokenData = PluginTokenData();
        emit store(d->mTokenData);
    }

    d->mUsername = inData.UserName();
    d->mPassword = inData.Secret();

    if (!d->mTokenData.Token().isEmpty() && !d->mTokenData.SWAPIServer().isEmpty() &&
        d->mTokenData.TokenExpires() > QDateTime::currentDateTimeUtc()) {
        /* Valid WebEx token found. Try to acquire the Jabber token from it. */
        d->mTriedSSO = false;
        d->retrieveJabberToken();
    }

    d->mTriedSSO = true;
    d->performWebexSSO();
}

void Plugin::userActionFinished(const SignOn::UiSessionData &data)
{
    Q_D(Plugin);

    if (data.QueryErrorCode() != QUERY_ERROR_NONE) {
        if (data.QueryErrorCode() == QUERY_ERROR_CANCELED)
            emit error(Error(Error::SessionCanceled, QStringLiteral("Cancelled by user")));
        else
            emit error(Error(Error::UserInteraction,
                             QStringLiteral("userActionFinished error: ")
                             + QString::number(data.QueryErrorCode())));
        return;
    }

    QString content = data.WebContent();
    if (content.isEmpty()) {
        emit error(Error(Error::MissingData, QStringLiteral("Authentication XML response empty.")));
        return;
    }

    int pos = content.indexOf(QStringLiteral("<federatedSSO>"));
    if (pos < 0) {
        emit error(Error(Error::MissingData, QStringLiteral("Incorrect SSO XML content.")));
        return;
    }

    QDomDocument doc;
    if (!doc.setContent(content.mid(pos), false)) {
        emit error(Error(Error::MissingData, QStringLiteral("Failed to parse SSO XML.")));
        return;
    }

    QDomNode root = doc.firstChildElement(QStringLiteral("federatedSSO"));
    if (root.firstChildElement(QStringLiteral("status")).text().trimmed() != QStringLiteral("SUCCESS"))
    {
        emit error(Error(Error::NotAuthorized, root.firstChildElement(QStringLiteral("errorMessage")).text().trimmed()));
        return;
    }

    bool ok;
    ulong createTime = root.firstChildElement(QStringLiteral("createtime")).text().trimmed().toULong(&ok);
    if (!ok) {
        emit error(Error(Error::MissingData, QStringLiteral("Incorrect token create time.")));
    }
    ulong ttl = root.firstChildElement(QStringLiteral("timetolive")).text().trimmed().toULong(&ok);
    if (!ok) {
        emit error(Error(Error::MissingData, QStringLiteral("Incorrect token TTL.")));
    }
    qDebug() << "Ticket expires" << QDateTime::fromMSecsSinceEpoch(createTime).addSecs(ttl);

    d->mTokenData.setToken(root.firstChildElement(QStringLiteral("token")).text().trimmed());
    d->mTokenData.setSWAPIServer(root.firstChildElement(QStringLiteral("serviceurl")).text().trimmed());
    d->mTokenData.setJabberServer(root.firstChildElement(QStringLiteral("xmppjabbercluster")).text().trimmed());
    d->mTokenData.setTokenExpires(QDateTime::fromMSecsSinceEpoch(createTime).addSecs(ttl));

    Q_EMIT store(d->mTokenData);

    d->retrieveJabberToken();
}

void Plugin::refresh(const SignOn::UiSessionData &data)
{
    Q_EMIT refreshed(data);
}
    
}

#include "plugin.moc"

