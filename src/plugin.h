/*
 * This file is part of signon WebEx plugin
 *
 * Copyright (C) 2016 Krzysztof Nowicki <krissn@op.pl>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef SIGNON_PLUGIN_WEBEX_H
#define SIGNON_PLUGIN_WEBEX_H

#include <QtCore/QScopedPointer>
#include <SignOn/AuthPluginInterface>

namespace SignOnWebex {

class PluginPrivate;

class Plugin : public AuthPluginInterface
{
    Q_OBJECT
    Q_INTERFACES(AuthPluginInterface)
public:
    Plugin(QObject *parent = Q_NULLPTR);
    ~Plugin();

public Q_SLOTS:
    QString type() const;
    QStringList mechanisms() const;
    void cancel();
    void process(const SignOn::SessionData &inData, const QString &mechanism = 0);
    void userActionFinished(const SignOn::UiSessionData &data);
    void refresh(const SignOn::UiSessionData &data);
private:
    QScopedPointer<PluginPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Plugin)
};
    
}

#endif
