/*
 * This file is part of signon WebEx plugin
 *
 * Copyright (C) 2016 Krzysztof Nowicki <krissn@op.pl>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef SIGNON_PLUGINDATA_WEBEX_H
#define SIGNON_PLUGINDATA_WEBEX_H

#include <SignOn/SessionData>

namespace SignOnWebex {

class PluginTokenData : public SignOn::SessionData
{
public:
    SIGNON_SESSION_DECLARE_PROPERTY(QString, Token);
    SIGNON_SESSION_DECLARE_PROPERTY(QDateTime, TokenExpires);
    SIGNON_SESSION_DECLARE_PROPERTY(QString, SWAPIServer);
    SIGNON_SESSION_DECLARE_PROPERTY(QString, JabberServer);
    SIGNON_SESSION_DECLARE_PROPERTY(QString, JabberToken);
};

}

#endif
