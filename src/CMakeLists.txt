include_directories(${SIGNONQT_INCLUDE_DIRS} ${SIGNON_PLUGINS_INCLUDE_DIRS})

set(signon_webex_SRCS
    plugin.cpp
)

set(signon_webex_LIBS
    ${SIGNONQT_LIBRARIES}
    ${SIGNON_PLUGINS_LIBRARIES}
    Qt5::Core
    Qt5::Network
    Qt5::Xml
)

add_library(webexplugin SHARED ${signon_webex_SRCS})
target_link_libraries(webexplugin ${signon_webex_LIBS})

install(TARGETS webexplugin DESTINATION ${CMAKE_INSTALL_LIBDIR}/signon/)

