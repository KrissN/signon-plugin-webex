WebEx plugin for the SignOn daemon
==================================

This plugin handles Cisco WebEx authentication (WEBEX-TOKEN). It was developed for use with Cisco Jabber.

License
-------

See COPYING file.

Build instructions
------------------

This project depends on Qt 5 and [signond](https://gitlab.com/accounts-sso/signond). To build it, just run
```
  cmake .
  make
  make install
```